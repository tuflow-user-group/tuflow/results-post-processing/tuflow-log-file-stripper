"""
/***************************************************************************
Ths script will allow you to open a TUFLOW tlf file and strip out messages
that can occur in large quantities, making the tlf unwieldy and difficult
to view.
                             -------------------
        begin                : 2021-11-04
        copyright            : (C) 2021 by Duncan Kitts
 ***************************************************************************/
"""

import os
from tkinter import Tk, simpledialog
from tkinter.filedialog import askopenfilename

root = Tk()
tlf_name = askopenfilename(filetypes=[("TUFLOW TLF File", "*.tlf")], title="Select TUFLOW Log File to be stripped")
message = str(simpledialog.askstring(title="TLF Message",
                                            prompt="What message would you like stripped (e.g. WARNING 2550 or CHECK 1424)?"))
message_number = message[-4:]

tlf_stripped_name = (os.path.splitext(os.path.basename(tlf_name))[0])
output_directory = os.path.split(os.path.abspath(tlf_name))[0]
root.withdraw()

def stripper(file_name):
    with open(output_directory+("/")+tlf_stripped_name+'_'+message+"_stripped.tlf", "w") as output:
        with open(file_name) as file:
            for line in file:
                if not line.strip("\n").startswith('XY: '+message):
                    output.write(line)
        output.write('\n *Note: A large number of "XY: '+message+' -" messages have been stripped from the log file to make it easier to manage.  See https://wiki.tuflow.com/TUFLOW_Message_'+message_number+' to find out more about the warning message.')

stripper(tlf_name)